package com.test.web.repository;

import com.test.web.entity.TestUserEntiry;
import io.micronaut.data.annotation.Repository;
import io.micronaut.data.repository.CrudRepository;

@Repository
public interface TestUserRepository extends CrudRepository<TestUserEntiry, Long> {

}
