package com.test.web.controller;

import com.test.web.entity.TestUserEntiry;
import com.test.web.repository.TestUserRepository;
import io.micronaut.http.annotation.Body;
import io.micronaut.http.annotation.Controller;
import io.micronaut.http.annotation.Get;
import io.micronaut.http.annotation.Post;
import io.micronaut.json.tree.JsonObject;

@Controller("/test")
public class TestUserController {

    private final TestUserRepository testUserRepository;

    public TestUserController(TestUserRepository testUserRepository) {
        this.testUserRepository = testUserRepository;
    }

    @Get("/findAll")
    public Iterable<TestUserEntiry> test() {
        return testUserRepository.findAll();
    }

    @Post("/add")
    public boolean addUser(@Body JsonObject json) {
        testUserRepository.save(new TestUserEntiry(json.get("username").getStringValue(), json.get("age").getIntValue()));
        return true;
    }
}
