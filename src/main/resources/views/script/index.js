$.ajax({
    type : "POST",
    url: '/test/add',
    data: JSON.stringify({username: "测试用户", age: 18}),
    headers: {token: localStorage.getItem('token')},
    dataType : "json",
    contentType : 'application/json',
    success: function(res) {
        console.info(res);
    },
    error: function(e) {console.log(e)}
});